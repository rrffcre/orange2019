#include "variables.cpp"

using namespace pros::c;

void opcontrol() {
	while(true) {
		lcd_print(0, "Gyro Value: %d\n", gyro_value); //Puts gyro value on screen...
    /*DECLARES MOTOR AS REVERSED OR NOT*/
		motor_set_reversed(driveFLPort, 1); //Reverses motor...
		motor_set_reversed(driveBLPort, 1);
		motor_set_reversed(driveFRPort, 1);
		motor_set_reversed(driveBRPort, 1);
    motor_set_reversed(puncherPort, 0);
    motor_set_reversed(flipperPort, 1);
    motor_set_reversed(intakePort, 1);

    /*DECLARES THE GEARING OF THE MOTOR*/
    motor_set_gearing(intakePort, pros::E_MOTOR_GEARSET_06); //600rpm cartridge
		motor_set_gearing(driveFLPort, pros::E_MOTOR_GEARSET_18); //200rpm cartridge
		motor_set_gearing(driveBLPort, pros::E_MOTOR_GEARSET_18);
		motor_set_gearing(driveFRPort, pros::E_MOTOR_GEARSET_18);
		motor_set_gearing(driveBRPort, pros::E_MOTOR_GEARSET_18);
    motor_set_gearing(puncherPort, pros::E_MOTOR_GEARSET_36); //100rpm cartridge
    motor_set_gearing(flipperPort, pros::E_MOTOR_GEARSET_36);

    /*DECLARES THE BRAKING MODE OF THE MOTOR*/
    motor_set_brake_mode(puncherPort, pros::E_MOTOR_BRAKE_HOLD);
    motor_set_brake_mode(flipperPort, pros::E_MOTOR_BRAKE_COAST);
    motor_set_brake_mode(intakePort, pros::E_MOTOR_BRAKE_COAST);

    /*DECLARES ENCODER UNITS OF THE MOTOR*/
    motor_set_encoder_units(puncherPort, pros::E_MOTOR_ENCODER_DEGREES);

		int driveLF = (RjoystickX + LjoystickX + RjoystickY);
		int driveLB = (RjoystickX - LjoystickX - RjoystickY);
		int driveRF = (RjoystickX + LjoystickX - RjoystickY);
		int driveRB = (RjoystickX - LjoystickX + RjoystickY);

		if (driveLF > 200){ //If drive input is greater than 200, change it to 200
      driveLF = 200;
  	}
  	if (driveRF > 200){
  		driveRF = 200;
  	}
  	if (driveLF < -200){ //If drive input is less than 200, change it to 200
  		driveLF = -200;
  	}
  	if (driveRF < -200){
  		driveRF = -200;
  	}
    if (driveLB > 200){
  		driveLB = 200;
  	}
  	if (driveRB > 200){
  		driveRB = 200;
  	}
  	if (driveLB < -200){
  		driveLB = -200;
  	}
  	if (driveRB < -200){
  		driveRB = -200;
  	}
		motor_move(driveFLPort, driveLF);
  	motor_move(driveBLPort, driveLB);
  	motor_move(driveFRPort, driveRF);
  	motor_move(driveBRPort, driveRB);

		//PUNCHER CONTROLS...
		if (master_x || partner_x){ //Manual control...
			motor_move_velocity(puncherPort, -100);
		} else if (master_b || partner_b){
      motor_move_velocity(puncherPort, 100);
    } else if (master_y || partner_y){
      motor_move_absolute(puncherPort, 541, 200);
    } else if (master_a || partner_a){
      motor_move_absolute(puncherPort, 500, 200);
    } else {
      motorStop(puncherPort);
    }

    //BALL INTAKE CONTROLS...
    if (partner_l1 || master_l1){
      motor_move_velocity(intakePort, 600);
    } else if (partner_l2 || master_l2){
      motor_move_velocity(intakePort, -350);
    } else {
      motorStop(intakePort);
    }

		//CAP-FLIPPER CONTROLS...
		if (partner_r1 || master_r1){
			motor_move_velocity(flipperPort, 200);
		} else if (partner_r2 || master_r2){
			motor_move_velocity(flipperPort, -200);
		} else {
			motorStop(flipperPort);
		}

    //TREAD CONTROLS...
    if (partner_r1 || master_r1){
      motor_move_velocity(treadPort, 200);
    } else if (partner_r2 || master_r2){
      motor_move_velocity(treadPort, -200);
    } else {
      motorStop(treadPort);
    }
	}
};
