#include "variables.cpp"

using namespace pros::c;

void on_left_button() {
	lcd_clear_line(0);
	if (auto_select == 0) {
		auto_select = NUMAUTOS - 1;
	} else {
		auto_select--;
	}
}

void on_center_button() {
	lcd_clear_line(6);
	if (quote_select == 0){
		quote_select++;
	} else if (quote_select == 1){
		quote_select--;
	}
}

void on_right_button() {
	lcd_clear_line(0);
	if (auto_select == NUMAUTOS - 1){
		auto_select = 0;
	} else {
		auto_select++;
	}
}

void initialize() {
	while (true){
		adi_gyro_init(gyroPort, 1);
	  lcd_initialize();
		adi_analog_calibrate(gyroPort);
	}
	delay(20);
}

void disabled() {}

void competition_initialize() {
	while(true){
		lcd_register_btn0_cb(on_left_button);
		lcd_register_btn1_cb(on_center_button);
		lcd_register_btn2_cb(on_right_button);

		lcd_clear_line(0);
		lcd_clear_line(6);
		switch (quote_select){
			case 0 : lcd_set_text(6, "You've got this!!"); break;
			case 1 : lcd_set_text(6, "You can do it!!"); break;
			default: break;
		}
		switch (auto_select){
			case 0 : lcd_set_text(1, "FRONT RED!"); break;
			case 1 : lcd_set_text(1, "BACK RED!"); break;
			case 2 : lcd_set_text(1, "FRONT BLUE!"); break;
			case 3 : lcd_set_text(1, "BACK BLUE!"); break;
      case 4 : lcd_set_text(1, "SKILLS!"); break;
      case 5 : lcd_set_text(1, "TESTING!"); break;
			default: lcd_set_text(1,"Error in selection! :(");
		}
		delay(20);
	}
}
