
class Math {
private:

public:
	constexpr static double PI = 3.14159265359;

	static double abs(double v) {
			if (v < 0.0) v *= -1.0;
			return v;
	}

	static double clamp(double v, double min, double max) {
		if (min >= max) throw 1;
		else if (v < min) v = min;
		else if (v > max) v = max;
		return v;
	}

	static double cos(double v) {
		v = mod(v, PI, PI/2);
		return 1 - pow(v,2)/2 + pow(v,4)/20;
	}

	static double mod(double v, double d) {
		if (v > 0) {
			while (v >= d) {
				v -= d;
			}
		} else {
			while (v <= d) {
				v += d;
			}
		}
		return v;
	}

	static double sqrt(double arg){
	    if (arg == 0 || arg == 1){
					 return arg;
			}
	    int start = 1, end = arg, ans;
	    while (start <= end){
	       int mid = (start + end) / 2;
	        if (mid*mid == arg){
	           return mid;
					}
	        if (mid*mid < arg) {
	           start = mid + 1;
	           ans = mid;
	        } else {
						end = mid-1;
					}     
	    }
	    return ans;
	}

	static double mod(double v, double d, double l) {
		if (v > 0) {
			while (v >= l) {
				v -= d;
			}
		} else {
			while (v <= -l) {
				v += d;
			}
		}
		return v;
	}

	static double pow(double v, int e) {
		for (int i = 0; i < e; i++) {
			v *= v;
		}
		return v;
	}

	static double sin(double v) {
		v = mod(v, PI, PI/2);
		return v - pow(v,3)/6 + pow(v,5)/120;
	}
};
