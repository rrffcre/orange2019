
class EdgeTrigger {
	private:
		bool state = false;

	public:
		bool updatePos(bool newState) {
			bool result = (!state and newState);
			this->state = newState;
			return result;
		}

		bool updateNeg(bool newState) {
			bool result = (state and !newState);
			this->state = newState;
			return result;
		}
};
