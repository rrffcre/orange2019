#include "variables.cpp"

using namespace pros::c;

float gyro_value = adi_analog_read_calibrated_HR(gyroPort);

void GyroTurn(int left, int right, int strafe, float deg){
  adi_gyro_reset(gyroPort);
  if (deg == 0.0){
    motor_move_velocity(driveFLPort, (left + strafe));
    motor_move_velocity(driveBLPort, (left - strafe));
    motor_move_velocity(driveFRPort, (right - strafe));
    motor_move_velocity(driveBRPort, (right + strafe));
  } else if (deg <= 180){
    while (gyro_value <= deg){
      //Assuming this is the polarity necessary for a clockwise turn
      motor_move_velocity(driveFLPort, left);
      motor_move_velocity(driveBLPort, left);
      motor_move_velocity(driveFRPort, -right);
      motor_move_velocity(driveBRPort, -right);

      delay(10);
    }
    //Stop motors and reset gyro after degree turn is reached...
    motorStop(driveFLPort);
    motorStop(driveBLPort);
    motorStop(driveFRPort);
    motorStop(driveBRPort);
    adi_gyro_reset(gyroPort);
  } else if (deg > 180){
    while (gyro_value >= deg){
      //Assuming this is the polarity needed for a counter-clockwise turn
      motor_move_velocity(driveFLPort, -left);
      motor_move_velocity(driveBLPort, -left);
      motor_move_velocity(driveFRPort, right);
      motor_move_velocity(driveBRPort, right);

      delay(10);
    }
    //Stop motors and reset gyro after degree turn is reached...
    motorStop(driveFLPort);
    motorStop(driveBLPort);
    motorStop(driveFRPort);
    motorStop(driveBRPort);
    adi_gyro_reset(gyroPort);
  }
}
