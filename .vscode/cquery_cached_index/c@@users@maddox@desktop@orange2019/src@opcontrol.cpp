#include "..\src\variables.cpp"

using namespace pros::c;

typedef struct {
        bool current;
        bool last;

        bool changed;
        bool posEdge;
        bool negEdge;
    } EDGE;
    EDGE edgeManual;
    EDGE edgeFire;
    EDGE edgeHigh;
    EDGE edgeLow;

void executeEdge(EDGE *e) {
  e->posEdge = e->current && !e->last;
  e->negEdge = !e->current && e->last;
  e->changed = e->posEdge || e->negEdge;
  e->last = e->current;
}

void opcontrol() {
	while(true) {
		lcd_print(0, "Gyro Value: %d\n", gyro_value);

    /*DECLARES MOTOR AS REVERSED OR NOT*/
		motor_set_reversed(driveFLPort, 1); //Reverses motor...
		motor_set_reversed(driveBLPort, 1);
		motor_set_reversed(driveFRPort, 1);
		motor_set_reversed(driveBRPort, 1);
    motor_set_reversed(puncherPort, 0);
    motor_set_reversed(flipperPort, 1);
    motor_set_reversed(intakePort, 1);

    /*DECLARES THE GEARING OF THE MOTOR*/
    motor_set_gearing(intakePort, pros::E_MOTOR_GEARSET_06); //600rpm cartridge
		motor_set_gearing(driveFLPort, pros::E_MOTOR_GEARSET_18); //200rpm cartridge
		motor_set_gearing(driveBLPort, pros::E_MOTOR_GEARSET_18);
		motor_set_gearing(driveFRPort, pros::E_MOTOR_GEARSET_18);
		motor_set_gearing(driveBRPort, pros::E_MOTOR_GEARSET_18);
    motor_set_gearing(puncherPort, pros::E_MOTOR_GEARSET_36); //100rpm cartridge
    motor_set_gearing(flipperPort, pros::E_MOTOR_GEARSET_36);

    /*DECLARES THE BRAKING MODE OF THE MOTOR*/
    motor_set_brake_mode(puncherPort, pros::E_MOTOR_BRAKE_HOLD);
    motor_set_brake_mode(flipperPort, pros::E_MOTOR_BRAKE_COAST);
    motor_set_brake_mode(intakePort, pros::E_MOTOR_BRAKE_COAST);

    /*DECLARES ENCODER UNITS OF THE MOTOR*/
    motor_set_encoder_units(puncherPort, pros::E_MOTOR_ENCODER_DEGREES);

		int driveLF = (RjoystickX + LjoystickX + RjoystickY);
		int driveLB = (RjoystickX - LjoystickX - RjoystickY);
		int driveRF = (RjoystickX + LjoystickX - RjoystickY);
		int driveRB = (RjoystickX - LjoystickX + RjoystickY);

		if (driveLF > 200){ //If drive input is greater than 200, change it to 200
      driveLF = 200;
  	}
  	if (driveRF > 200){
  		driveRF = 200;
  	}
  	if (driveLF < -200){ //If drive input is less than 200, change it to 200
  		driveLF = -200;
  	}
  	if (driveRF < -200){
  		driveRF = -200;
  	}
    if (driveLB > 200){
  		driveLB = 200;
  	}
  	if (driveRB > 200){
  		driveRB = 200;
  	}
  	if (driveLB < -200){
  		driveLB = -200;
  	}
  	if (driveRB < -200){
  		driveRB = -200;
  	}
		motor_move(driveFLPort, driveLF);
  	motor_move(driveBLPort, driveLB);
  	motor_move(driveFRPort, driveRF);
  	motor_move(driveBRPort, driveRB);

		//PUNCHER CONTROLS...
		edgeManual.current = partner_x || master_x;
    edgeFire.current = partner_b || master_b;
    edgeHigh.current = partner_a || master_a;
    edgeLow.current = partner_y || master_y;
		executeEdge(&edgeManual);
    executeEdge(&edgeFire);
    executeEdge(&edgeHigh);
    executeEdge(&edgeLow);
		if (edgeManual.current){ //Manual control...
			motor_move_velocity(puncherPort, -100);
		} else if (edgeFire.current){
      motor_move_velocity(puncherPort, 100);
    } else if (edgeManual.negEdge){
			motor_move(puncherPort, 0);
		} else if (edgeHigh.current){
      motor_move_absolute(puncherPort, -400, 200);
    } else if (edgeLow.current){
      motor_move_absolute(puncherPort, -250, 200);
    }

    //BALL INTAKE CONTROLS...
    if (partner_l1 || master_l1){
      motor_move_velocity(intakePort, 600);
    } else if (partner_l2 || master_l2){
      motor_move_velocity(intakePort, -600);
    } else {
      motor_move(intakePort, 0);
    }

		//CAP-FLIPPER CONTROLS...
		if (partner_r1 || master_r1){
			motor_move_velocity(flipperPort, 200);
		} else if (partner_r2 || master_r2){
			motor_move_velocity(flipperPort, -200);
		} else {
			motor_move(flipperPort, 0);
		}

    //TREAD CONTROLS...
    if (partner_l1 || master_l1){
      motor_move_velocity(treadPort, 200);
    } else if (partner_l2 ||master_l2){
      motor_move_velocity(treadPort, -200);
    } else {
      motor_move(treadPort, 0);
    }
	}
};
