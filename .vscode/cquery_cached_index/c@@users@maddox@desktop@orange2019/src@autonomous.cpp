#include "..\src\Gyro.cpp"

using namespace pros::c;

void step_end(){ //Sets final position to zero for opcontrol.
  motor_tare_position(driveFRPort);
  motor_tare_position(driveBRPort);
  motor_tare_position(driveFLPort);
  motor_tare_position(driveBLPort);
  motor_tare_position(puncherPort);
  motor_tare_position(flipperPort);
  adi_gyro_reset(gyroPort);
  controller_clear(CONTROLLER_MASTER);
  auto_count++;
}

void step_calc(int duration, int sensor){
  if (sensor == TIME){ //Delay for specified amount of time...
    delay(duration);
    step_end();
  } else if (sensor == ENCO){ //Move based on the front drive currents...
      if (abs(driveFLCurrent) > duration && abs(driveFRCurrent) > duration){
        step_end();
    }
  }
}

void auto_step(int left, int right, int strafe, float turn, /*int tread,*/ int punch, int intake, int cap, int duration, int sensor){
  motor_move_velocity(intakePort, intake);
  // motor_move_velocity(treadPort, tread);
  motor_move_absolute(flipperPort, cap, 150);
  motor_move(puncherPort, punch);
  GyroTurn(left, right, strafe, turn);
  step_calc(duration, sensor);
}

void autonomous() {
  while (true){
    motor_set_reversed(driveFLPort, 1); //Reverses motor...
    motor_set_reversed(driveBLPort, 0);
    motor_set_reversed(driveFRPort, 0);
    motor_set_reversed(driveBRPort, 1);
    motor_set_encoder_units(puncherPort, pros::E_MOTOR_ENCODER_DEGREES);
    motor_set_encoder_units(flipperPort, pros::E_MOTOR_ENCODER_ROTATIONS);
    if (auto_select == 0){ //FRONT RED
       switch(auto_count){
  		   case 0 : step_end(); break;
         case 1 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 2 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 3 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 4 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 5 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
  		   default: return;
		     }
     } else if (auto_select == 1){ //BACK RED
         switch(auto_count){
        case 0 : step_end(); break;
        case 1 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
        case 2 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
        case 3 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
        case 4 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
        case 5 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
        default: return;
      }
	   } else if (auto_select == 2){ //BACK BLUE
        switch(auto_count){
  		   case 0 : step_end(); break;
         case 1 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 2 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 3 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 4 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 5 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         default: return;
  		}
	   } else if (auto_select == 3){ //FRONT BLUE
       switch(auto_count){
  		   case 0 : step_end(); break;
         case 1 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 2 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 3 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 4 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 5 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         default: return;
  		   }
	   } else if (auto_select == 4){ //SKILLS
        switch(auto_count){
  		   case 0 : step_end(); break;
         case 1 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 2 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 3 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 4 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         case 5 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
         default: return;
  		}
	   } else if (auto_select == 5){ //TESTING
        switch(auto_count){
          case 0 : step_end(); break;
          case 1 : auto_step( 200, 200,   0,   0.0,   0,   0,   0, 500, TIME); break;//Move forward
          case 2 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;//Brake
          case 3 : auto_step(   0,   0,   0,  90.0,   0,   0,   0, 600, TIME); break;//Turn 90 degrees
          case 4 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
          case 5 : auto_step(   0,   0,   0,   0.0,   0,   0,   0,   0, TIME); break;
          default: return;
       }
     }
 }
}
