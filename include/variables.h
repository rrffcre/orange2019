#include "main.h"

#define driveFLPort 1
#define driveBLPort 9
#define driveFRPort 2
#define driveBRPort 8
#define gyroPort 8
#define puncherPort 6
#define flipperPort 4
#define intakePort 5
#define treadPort 4

#define driveFLCurrent motor_get_position(driveFLPort)
#define driveFRCurrent motor_get_position(driveFRPort)
#define puncherCurrent motor_get_position(puncherPort)

// #define PRjoystickY  controller_get_analog(CONTROLLER_PARTNER, ANALOG_RIGHT_Y)
// #define PLjoystickY  controller_get_analog(CONTROLLER_PARTNER, ANALOG_LEFT_Y)
#define RjoystickY controller_get_analog(CONTROLLER_MASTER, ANALOG_RIGHT_Y)
#define RjoystickX controller_get_analog(CONTROLLER_MASTER, ANALOG_RIGHT_X)
#define LjoystickX controller_get_analog(CONTROLLER_MASTER, ANALOG_LEFT_X)
#define partner_r1 controller_get_digital(CONTROLLER_PARTNER, DIGITAL_R1)==1
#define partner_r2 controller_get_digital(CONTROLLER_PARTNER, DIGITAL_R2)==1
#define partner_l1 controller_get_digital(CONTROLLER_PARTNER, DIGITAL_L1)==1
#define partner_l2 controller_get_digital(CONTROLLER_PARTNER, DIGITAL_L2)==1
#define master_x controller_get_digital(CONTROLLER_MASTER, DIGITAL_X)==1
#define master_b controller_get_digital(CONTROLLER_MASTER, DIGITAL_B)==1
#define master_y controller_get_digital(CONTROLLER_MASTER, DIGITAL_Y)==1
#define master_a controller_get_digital(CONTROLLER_MASTER, DIGITAL_A)==1
#define master_up controller_get_digital(CONTROLLER_MASTER, DIGITAL_UP)==1
#define master_down controller_get_digital(CONTROLLER_MASTER, DIGITAL_DOWN)==1
#define master_left controller_get_digital(CONTROLLER_MASTER, DIGITAL_LEFT)==1
#define master_right controller_get_digital(CONTROLLER_MASTER, DIGITAL_RIGHT)==1
#define master_l1 controller_get_digital(CONTROLLER_MASTER, DIGITAL_L1)==1
#define master_l2 controller_get_digital(CONTROLLER_MASTER, DIGITAL_L2)==1
#define master_r1 controller_get_digital(CONTROLLER_MASTER, DIGITAL_R1)==1
#define master_r2 controller_get_digital(CONTROLLER_MASTER, DIGITAL_R2)==1
#define partner_b controller_get_digital(CONTROLLER_PARTNER, DIGITAL_B)==1
#define partner_x controller_get_digital(CONTROLLER_PARTNER, DIGITAL_X)==1
#define partner_y controller_get_digital(CONTROLLER_PARTNER, DIGITAL_Y)==1
#define partner_a controller_get_digital(CONTROLLER_PARTNER, DIGITAL_A)==1
#define partner_up controller_get_digital(CONTROLLER_PARTNER, DIGITAL_UP)==1
#define partner_down controller_get_digital(CONTROLLER_PARTNER, DIGITAL_DOWN)==1
#define partner_left controller_get_digital(CONTROLLER_PARTNER, DIGITAL_LEFT)==1
#define partner_right controller_get_digital(CONTROLLER_PARTNER, DIGITAL_RIGHT)==1

#define TIME 0
#define ENCO 1
#define PNCH 2
#define NUMAUTOS 6
#define GYRO_MULTI 1
extern int auto_count;
extern int auto_select;
extern int quote_select;
extern float gyro_value;
